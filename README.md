# pgdyn

Script meant to convert the tables & contents of a PgSQL database to a JSON structure that can be then imported in a AWS DynamoDB.
Each table in the DB will be stored in it's individual file.


# Usage
1. Create python virtualenv and activate it (optional but recommended)
2. Import requirements:
```
pip install -r requirements.txt
```

3. Run the script:
```
python pgdyn.py -s <pgsql_server> -u <pgsql_username> -p <pgsql_password> -d <database_name>
```

# Results
Will be stored in the output folder. Use those JSON files to import them into AWS DynamoDB as per [these instructions](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/SampleData.LoadData.html):
```
aws dynamodb batch-write-item --request-items file://<table_name>.json
```