import argparse
import decimal
# import boto3
import psycopg2
import json
import datetime
from os import mkdir
from os.path import isdir


__version_info__ = ('18', '09', '12')
__version__ = 'v0.1.' + ''.join(__version_info__)


def argParser():
    """
    Supports the command-line arguments listed below.
    :return the parser object and it's arguments; to be used in main()
    """

    # High-level parameters
    parser = argparse.ArgumentParser(description='This script generates JSON files for each table in '
                                                 'a PostgreSQL database using a DynamoDB importable format.',
                                     epilog='Use %(prog)s {command} -h to get help on individual commands')

    parser.add_argument('-V', '--version', action='version', version='%(prog)s {version}'.format(version=__version__))

    # Main input parameters
    parser.add_argument('-s', '--server', dest='server', action='store',
                        help="The server FQDN (or IP) that hosts your PgSQL database.")

    parser.add_argument('-u', '--user', dest='username', action='store',
                        help="The username used for connecting to the PgSQL database.")

    parser.add_argument('-p', '--pass', dest='password', action='store',
                        help="The password used for connecting to the PgSQL database.")

    parser.add_argument('-d', '--databse', dest='database', action='store',
                        help="The name of the source database on the PgSQL server.")

    args = parser.parse_args()

    return parser, args


def connectPgSQL(host, database, user, password, port=5432):
    try:
        connection = psycopg2.connect(host=host,
                                      database=database,
                                      user=user,
                                      password=password)
    except Exception as e:
        print("[!] Unable to connect to PgSQL; exiting")
        raise e
    else:
        # Get a cursor to run SQL queries
        return connection.cursor()


def getTables(cursor):
    # Get all tables
    try:
        cursor.execute("""SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'""")
    except Exception as e:
        print("[!] Unable to get the list of tables from the PgSQL database")
        raise e
    else:
        return cursor.fetchall()


def getTableData(cursor, tableName):
    # Get all entries from a Table
    try:
        # Execure the query
        cursor.execute("""SELECT * from {table}""".format(table=tableName))
    except Exception as e:
        print("[!] Unable to get the data from the \"%s\" table" % tableName)
        raise e
    else:
        # Retain them and close the conn to the DB engine
        data = cursor.fetchall()
        headers = [desc[0] for desc in cursor.description]
        return headers, data


if __name__ == "__main__":
    # Check output dir
    if not isdir('./output'):
        mkdir('./output')

    # Get the args
    parser, args = argParser()

    # Get the DB sessions
    cursor = connectPgSQL(host=args.server,
                          database=args.database,
                          user=args.username,
                          password=args.password)

    # Get all tables in the DB
    tables = getTables(cursor)

    # Save each table
    for tableName in tables:

        # Set the empty JSON struct
        tableName = tableName[0]
        jsonData = {}
        jsonData[tableName] = []

        # Get all the rows from PgSQL
        tableHeaders, tableData = getTableData(cursor, tableName)

        # Divide and conquer
        for tableEntry in tableData:
            # TODO Debug STOP here and see how an entry looks like
            dynItem = {}
            for i in range(1, len(tableHeaders)):
                key = tableHeaders[i]
                value = tableEntry[i].__str__()  # always be a string representation; DynamoDB seems to want it

                # Format the data types required by DynamoDB
                # S - the attribute is of type String
                # N - the attribute is of type Number
                # B - the attribute is of type Binary
                if isinstance(value, str):
                    dataType = "S"
                elif isinstance(value, datetime.date):
                    dataType = "S"
                elif isinstance(value, int):
                    dataType = "N"
                elif isinstance(value, float):
                    dataType = "N"
                elif isinstance(value, decimal.Decimal):
                    dataType = "N"
                else:
                    dataType = "B"

                dynItem[key] = {dataType: value}

            # Wrap it
            dynEntry = {
                "PutRequest": {
                    "Item": dynItem,
                }
            }

            # Add the entry to main stuct
            jsonData[tableName].append(dynEntry)

        # Dump to file, formatted
        with open("./output/%s.json" % tableName, 'w', encoding='utf-8') as f:
            json.dump(jsonData, f)
